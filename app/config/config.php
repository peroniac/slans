<?php
//DB Params
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'slans');

//App Root
define('APPROOT', dirname(dirname(__FILE__)));

//URL Root
define('URLROOT', 'http://slans.test');

//Site Name
define('SITENAME', 'Smart Laboratory Notification Systems');
