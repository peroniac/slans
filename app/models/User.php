<?php
class User
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    //User Login
    public function login($email, $password)
    {
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        $hashpassword = $row->password;

        if (password_verify($password, $hashpassword)) {
            return $row;
        } else {
            return false;
        }
    }

    //find user by email
    public function findUserByEmail($email)
    {
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email', $email);

        $row = $this->db->single();



        //check row
        if ($this->db->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //find user by username
    public function findUserByUsername($username)
    {
        $this->db->query('SELECT * FROM users WHERE username = :username');
        $this->db->bind('username', $username);

        $row = $this->db->single();



        //check row
        if ($this->db->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register($data)
    {
        $date = date('Y-m-d H:i:s');


        $this->db->query('INSERT INTO users (username, email, password, usercategoryid, createdby, createddate, updatedby, updateddate) VALUES (:username, :email, :password, 
        :usercategoryid, :createdby, :createddate, :updatedby, :updateddate)');


        //bind values
        $this->db->bind(':username', $data['username']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);
        $this->db->bind(':usercategoryid', 4); //auto registered as student

        $this->db->bind(':createdby', 1);
        $this->db->bind(':createddate', $date);
        $this->db->bind(':updatedby', 1);
        $this->db->bind(':updateddate', $date);

        //execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function resetPassword($data)
    {
        $date = date('Y-m-d H:i:s');

        $this->db->query('UPDATE users SET PASSWORD = :password, updateddate = :updateddate
        WHERE :email = :email');

        $this->db->bind('password', $data['passwordhashed']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('updateddate', $date);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
