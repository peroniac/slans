<?php require APPROOT . '/views/inc/loginheader.php'; ?>
<!--begin::Body-->

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
    <!--begin::Main-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Login-->
        <div class="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
            <!--begin::Aside-->
            <div class="login-aside order-2 order-lg-1 d-flex flex-row-auto position-relative overflow-hidden">
                <!--begin: Aside Container-->
                <div class="d-flex flex-column-fluid flex-column justify-content-between py-9 px-7 py-lg-13 px-lg-35">

                    <?php flash('register_success'); ?>
                    <!--begin::Logo-->
                    <a href="#" class="text-center pt-2">
                        <h1 class="card-title"> SLANS </h1>
                    </a>
                    <!--end::Logo-->
                    <!--begin::Aside body-->
                    <div class="d-flex flex-column-fluid flex-column flex-center">

                        <!--begin::Signup-->
                        <div class="login-form  pt-11">
                            <!--begin::Form-->
                            <form class="form" novalidate="novalidate" name="signupform" method="POST" action="<?= URLROOT; ?>/users/register">
                                <!--begin::Title-->
                                <div class="text-center pb-8">
                                    <h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Register</h2>
                                    <p class="text-muted font-weight-bold font-size-h4">Enter your details to create your account</p>
                                </div>
                                <!--end::Title-->
                                <!--begin::Form group-->
                                <div class="form-group">
                                    <input class="form-control <?php echo (!empty($data['username_err'])) ? 'is-invalid' : ''; ?> form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6" type="text" placeholder="Username" name="username" autocomplete="off" />
                                    <span class="invalid-feedback"><?= $data['username_err']; ?></span>
                                </div>
                                <!--end::Form group-->
                                <!--begin::Form group-->
                                <div class="form-group">
                                    <input class="form-control <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?> form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6" type="email" placeholder="Email" name="email" autocomplete="off" />
                                    <span class="invalid-feedback"><?= $data['email_err']; ?></span>
                                </div>
                                <!--end::Form group-->
                                <!--begin::Form group-->
                                <div class="form-group">
                                    <input class="form-control <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?> form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6" type="password" placeholder="Password" name="password" autocomplete="off" />
                                    <span class="invalid-feedback"><?= $data['password_err']; ?></span>
                                </div>
                                <!--end::Form group-->
                                <!--begin::Form group-->
                                <div class="form-group">
                                    <input class="form-control <?php echo (!empty($data['cpassword_err'])) ? 'is-invalid' : ''; ?> form-control-solid h-auto py-7 px-6 rounded-lg font-size-h6" type="password" placeholder="Confirm password" name="cpassword" autocomplete="off" />
                                    <span class="invalid-feedback"><?= $data['cpassword_err']; ?></span>
                                </div>
                                <!--end::Form group-->
                                <!--begin::Form group-->
                                <div class="form-group d-flex flex-wrap flex-center pb-lg-0 pb-3">
                                    <button type="submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">Submit</button>
                                    <a href="<?= URLROOT; ?>/users/login" class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">Cancel</a>
                                </div>
                                <!--end::Form group-->
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Signup-->

                    </div>
                    <!--end::Aside body-->
                </div>
                <!--end: Aside Container-->
            </div>
            <!--begin::Aside-->
            <!--begin::Content-->
            <div class="content order-1 order-lg-2 d-flex flex-column w-100 pb-0" style="background-color: #B1DCED;">
                <!--begin::Title-->
                <div class="d-flex flex-column justify-content-center text-center pt-lg-40 pt-md-5 pt-sm-5 px-lg-0 pt-5 px-7">
                    <h3 class="display4 font-weight-bolder my-7 text-dark" style="color: #986923;">SMART LABORATORY NOTIFICATION SYSTEM</h3>
                    <p class="font-weight-bolder font-size-h2-md font-size-lg text-dark opacity-70">Monitoring your chemical &amp; equipment by Smart Notification System
                    </p>
                </div>
                <!--end::Title-->
                <!--begin::Image-->
                <div class="content-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center" style="background-image: url(assets/media/svg/illustrations/login-visual-2.svg);"></div>
                <!--end::Image-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Login-->
    </div>
    <!--end::Main-->

    <?php require APPROOT . '/views/inc/loginfooter.php'; ?>