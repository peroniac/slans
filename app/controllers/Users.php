<?php

class Users extends Controller
{
    public function __construct()
    {

        //calling models
        $this->userModel = $this->model('User');
    }

    public function index()
    {
        $data = [
            'title' => 'Users',
            'date'  =>  date("Y/m/d")
        ];

        $this->view('users/index', $data);
    }

    public function login()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            //sanitize post data
            $data = [
                'title' => 'LOGIN | SLANS - SMART LABORATORY NOTIFICATION SYSTEM',
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => ''

            ];

            //validate email
            if (empty($data['email'])) {
                $data['email_err'] = 'Please enter email';
            }

            //validate password
            if (empty($data['password'])) {
                $data['password_err'] = 'Please enter password';
            }

            //check for email
            if ($this->userModel->findUserByEmail($data['email'])) {
                //user found
            } else {
                //user not found
                $data['email'] = 'No user found';
            }
            //make sure error are empty
            if (empty($data['email_err']) && empty($data['password_err'])) {
                //validated 
                //check login with model
                $loggedInUser = $this->userModel->login($data['email'], $data['password']);

                if ($loggedInUser) {
                    $this->createUserSession($loggedInUser);
                    //create session
                    // die('succes login');
                } else {

                    // die('error login');
                    $data['password_err'] = 'Password incorrect';
                    $this->view('users/login', $data);
                }


                //else 
                //error

            } else {
                //error
                $this->view('users/login', $data);
            }
        } else {
            $data = [
                'title' => 'LOGIN | SLANS - SMART LABORATORY NOTIFICATION SYSTEM'


            ];
        } // if not received method post 


        $this->view('users/login', $data);
    }

    public function register()
    {
        //if form post 
        if ($_SERVER["REQUEST_METHOD"] == 'POST') {

            //process form

            //sanitize post data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            //init data
            $data = [
                'username' => trim($_POST['username']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'cpassword' => trim($_POST['cpassword']),
                'username_err' => '',
                'email_err' => '',
                'password_err' => '',
                'cpassword_err' => ''

            ];




            //validate username
            if (empty($data['username'])) {
                $data['username_err'] = 'Please enter your username';
            } else {
                //check exist username
                if ($this->userModel->findUserByUsername($data['username'])) {
                    $data['username_err'] = 'username already exist';
                }
            }


            //validate email
            if (empty($data['email'])) {
                $data['email_err'] = 'Please enter your email';
            } else {
                //check exist email
                if ($this->userModel->findUserByEmail($data['email'])) {
                    $data['email_err'] = 'email already exist';
                }
            }
            //validate password
            /*Best Password Policy
            1. Must be at least 6 characters length
            2. Combination of small and capital letters
            3. Combination with numbers
            4. Combination with special characters
            */

            if (empty($data['password'])) {
                $data['password_err'] = 'Please enter your password';
            } elseif (strlen($data['password']) < 6) {
                $data['password_err'] = 'Password must be at least 6 characters';
            }

            if (empty($data['cpassword'])) {
                $data['cpassword_err'] = 'Please enter your password';
            } elseif ($data['password'] != $data['cpassword']) {
                $data['cpassword_err'] = 'Confirm Password not match with password';
            }
            //make sure error are empty
            if (
                empty($data['username_err']) && empty($data['email_err'])
                && empty($data['password_err']) && empty($data['cpassword_err'])
            ) {
                //no errors 

                //hash password
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

                //register user 
                if ($this->userModel->register($data)) {
                    flash('register_success', 'You are successfully registered. Please login.');
                    redirect('users/login');
                    //register model
                } else {
                    die('register errors');
                }
            } //make sure errors are empty

            else {
                //load view with errors
                $this->view('users/register', $data);
            }
        } //end of if for post 
        else {
            $data = [
                'title' => 'REGISTER | SLANS - SMART LABORATORY NOTIFICATION SYSTEM'


            ];
        } // if not received method post 


        $this->view('users/register', $data);
    } //end of public function register 


    public function forgotpassword()
    {
        if ($_SERVER['REQUEST_MWTHOD'] == 'POST') {

            //sanitize post data 
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            //init data
            $data = [
                'email' => trim($_POST['email']),
                'email_err' => ''
            ];

            //validate email
            if (empty($data['email'])) {
                $data['email_err'] = "please enter your email";
            } else {
                //check exist email
                if ($this->userModel->findUserByEmail($data['email'])) {
                    //1. generate new password
                    $data['password'] = generatePassword();

                    //2. hash the password
                    $data['passwordhashed'] = password_hash($data['password'], PASSWORD_DEFAULT);

                    //3. update the new password in the database
                    $this->userModel->resetPassword($data);

                    //4. email the new password

                    $to = $data['email'];
                    $subject = 'SLANS | YOUR PASSWORD RESET REQUEST';
                    $message = '<html>
                    <head>
                      <title>Your New Password</title>
                    </head>
                    <body>
                      <p>Here is your new password</p>
                      <br><br>
                      <p>Password: ' . $data['password'] . '</p>
                    </body>
                    </html>';

                    $headers = 'MIME-Version: 1.0 /r/n';
                    $headers .= 'Content-type: text/html; charset=iso-8859-1 /r/n';
                    $headers .= 'To: ' . $data['email'] . '/r/n';
                    $headers .= 'From: SLANS Admin <admin@slans.test> /r/n';

                    //default php send email function
                    $success = mail($to, $subject, $message, $headers);
                    if (!$success) {
                        $data['email_err'] = error_get_last()['message'];
                    } else {
                        redirect('users/login');
                    }
                } else {
                    $data['email'] = 'Email does not exist. Please register';
                }
            }
        } else {
            $data =
                [
                    'title' => 'REGISTER | SLANS - SMART LABORATORY NOTIFICATION SYSTEM'
                ];
        }
        $this->view('users/forgotpassword', $data);
    }

    public function createUserSession($user)
    {
        $_SESSION['userid'] = $user->userid;
        $_SESSION['email'] = $user->email;
        $_SESSION['username'] = $user->username;

        redirect('pages/index');
    }

    public function logout()
    {
        unset($_SESSION['userid']);
        unset($_SESSION['email']);
        unset($_SESSION['username']);

        session_destroy();

        redirect('users/login');
    }
}
