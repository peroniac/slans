<?php
class Pages extends Controller
{
	public function __construct()
	{
	}

	public function index()
	{
		//check if user is logged in
		/* if (!isloggedin()) {
			redirect('users/login');
		} */

		$data =
			[
				'title' => 'SLANS - SMART LABORATORY NOTIFICATION SYSTEM',
				'date' => '26 Jun 2021'
			];


		$this->view('pages/index', $data);
	}

	public function about()
	{
		$data = [
			'title' => 'About Us'
		];

		//echo "this about page";
		$this->view('pages/about', $data);
	}

	public function contact()
	{

		echo "This is a contact page";
	}
}
