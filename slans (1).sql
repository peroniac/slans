-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 10, 2021 at 10:38 AM
-- Server version: 5.7.33
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `slans`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(12) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `usercategoryid` int(11) NOT NULL,
  `createddate` timestamp NOT NULL,
  `createdby` int(12) NOT NULL,
  `updateddate` timestamp NOT NULL,
  `updatedby` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `username`, `password`, `email`, `usercategoryid`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 'admin', '$2y$10$noJAlqCuR89c/Kv1NsjKS.5BHtNCqaGin19cr7Y/aA1KWTCY8Bv3S', 'uminajihah0128@gmail.com', 1, '2021-07-02 16:00:00', 1, '2021-07-02 16:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_category`
--

CREATE TABLE `users_category` (
  `usercategoryid` int(11) NOT NULL,
  `usercategoryname` varchar(255) NOT NULL,
  `createdby` int(11) NOT NULL,
  `createddate` timestamp NOT NULL,
  `updatedby` int(11) NOT NULL,
  `updateddate` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_category`
--

INSERT INTO `users_category` (`usercategoryid`, `usercategoryname`, `createdby`, `createddate`, `updatedby`, `updateddate`) VALUES
(1, 'superadmin', 1, '2021-07-06 16:00:00', 1, '2021-07-06 16:00:00'),
(2, 'lab executive', 1, '2021-07-06 16:00:00', 1, '2021-07-06 16:00:00'),
(3, 'lab assistant', 1, '2021-07-06 16:00:00', 1, '2021-07-06 16:00:00'),
(4, 'student', 1, '2021-07-06 16:00:00', 1, '2021-07-06 16:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `users_category`
--
ALTER TABLE `users_category`
  ADD PRIMARY KEY (`usercategoryid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_category`
--
ALTER TABLE `users_category`
  MODIFY `usercategoryid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
